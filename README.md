# Philippe Demerliac CP0104

reprise de [http://philippe.demerliac.free.fr/Articles/CP0104/CP0104](http://philippe.demerliac.free.fr/Articles/CP0104/CP0104)

# Re-dessin & design PCB à ma façon

Parce que je préfère kicad, et que si je fais des modifications pour mes besoins, ça me fera une base facile.

Dans la version initiale v0.0 je ne vais rien changer au schéma proposé par Philippe Demerliac.

Je prends bien sur la version utilisant l'option de stabilité en température autour d'un TL431 telle que proposée.

# Schema

[schema en pdf](./cp0104/cp0104.pdf)

# PCB d'exemple 

ceci n'est qu'un exemple fonction des connecteurs et supports fusibles et heatsink que j'ai en stock

![plan de masse](./cp0104/cp0104-copper.png)

![coté composants](./cp0104/cp0104-up.png)

# Fichiers kicad

[consultables ici](./cp0104/)


